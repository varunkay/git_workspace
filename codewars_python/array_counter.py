def count(array):
    return {a: array.count(a) for a in array}
